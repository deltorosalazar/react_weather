import React, { Component } from 'react';
import { Link, IndexLink } from 'react-router';

class Navbar extends Component {
    render() {
        return (
            <nav className="navbar navbar-default">
                <div className="container">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="#">My Weather App</a>
                    </div>

                    <div className="collapse navbar-collapse" id="navbar-collapse-1">
                        <ul className="nav navbar-nav navbar-right">

                            <li>
                                <IndexLink to="/" activeClassName="active">Home</IndexLink>
                            </li>
                            <li>
                                <Link to="about" activeClassName="active">About</Link>
                            </li>
                            <li>
                                <Link to="examples" activeClassName="active">Examples</Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}

export default Navbar;
