import React, { Component } from 'react';

class WeatherForm extends Component {
    // constructor() {
    //
    // }

    onFormSubmit(e) {
        e.preventDefault();

        var location = this.refs.location.value;

        if (location.length > 0) {
            this.refs.location.value = "";
            this.props.onSearch(location);
        }
    }

    render() {
        return (
            <div className="row">
                <h1>Get Weather</h1>
                <br/>
                <form onSubmit={this.onFormSubmit.bind(this)}>
                    <div className="form-group">
                        <input type="text" className="form-control" placeholder="Enter City Name" ref="location"/>
                    </div>
                    <div className="form-group">
                        <button className="btn btn-success">Get Weather</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default WeatherForm;
