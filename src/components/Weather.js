import React, {Component} from 'react';
import WeatherForm from './WeatherForm';
import WeatherMessage from './WeatherMessage';
import { getTemp } from './../api/openWeatherMap';

class Weather extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false
        }
    }

    handleSearch(location) {
        var that = this;

        this.setState({
            isLoading: true
        });

        getTemp(location).then(function(temp) {
            // The this get lost inside the function, that's why we use that
            that.setState({
                isLoading: false,
                location: location,
                temp: temp
            });
        }, function(errorMessage) {
            alert(errorMessage);
        });
    }

    render() {
        var { isLoading, location, temp } = this.state;

        function renderMessage() {
            if (isLoading) {
                return <h3>Fetching Weather</h3>;
            } else if(temp && location) {
                return <WeatherMessage location={ location } temp={ temp }/>;
            }
        }

        return(
            <div className="container">
                <div className="col-md-offset-4 col-md-4">
                    <WeatherForm onSearch={ this.handleSearch.bind(this) }/>
                    { renderMessage() }
                </div>
            </div>

        );
    }
}

export default Weather;
