import React, { Component } from 'react';

class WeatherForm extends Component {
    // constructor() {
    //
    // }

    render() {
        var { temp, location } = this.props;

        return (
            <h2>It is { temp } ºC in { location }</h2>
        );
    }
}

export default WeatherForm;
