import React, { Component } from 'react';
import Navbar from './Navbar';

class Main extends Component {
    // constructor() {
    //
    // }

    render() {
        return (
            <div>
                <Navbar />                
                {this.props.children}
            </div>
        );
    }
}

export default Main;
