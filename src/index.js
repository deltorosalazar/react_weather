import React from 'react';
import ReactDOM from 'react-dom';
import {Route, Router, IndexRoute, history} from 'react-router';
import registerServiceWorker from './registerServiceWorker';

import Main from './components/Main';
import Weather from './components/Weather';
import About from './components/About';
import Examples from './components/Examples';


ReactDOM.render(
    <Router history={history}>
        <Route path="/" component={Main}>
            <Route path="about" component={About}/>
            <Route path="examples" component={Examples}/>
            <IndexRoute component={Weather} />
        </Route>
    </Router>,

    document.getElementById('root'));
registerServiceWorker();
