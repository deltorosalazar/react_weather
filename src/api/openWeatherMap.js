import axios from 'axios';

const OPEN_WEATHER_MAP_URL = 'http://api.openweathermap.org/data/2.5/weather?appid=446714f1bb9c0e7537b09af7f07f26be';

export function getTemp(location) {
    var encodedLocation = encodeURIComponent(location);
    var requestUrl = `${OPEN_WEATHER_MAP_URL}&q=${location}&units=metric`;

    return axios.get(requestUrl).then(function(response) {
        // console.log(response);
        
        if (response.data.cod && response.data.message) {
            throw new Error(response.data.message);
        } else {
            return response.data.main.temp;
        }
    }, function(response) {
        throw new Error(response.data.message);
    })
}
